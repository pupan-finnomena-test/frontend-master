import Vue from 'vue';
import VueRouter from 'vue-router';

import Layout from '@/components/Layout/Layout';

import Dashboard from '@/pages/Dashboard/Dashboard';

import Error from "@/pages/Error/Error";
// import Login from "@/pages/Login/Login";

 



Vue.use(VueRouter);


const routes = [

  {
    path: 'dashboard',
    name: 'Dashboard',
    component: Dashboard,
  },

  {
  path: '/',
  name: 'Layout',
  component: Layout,
  children: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard,
    },
   
  
  ],
},
  {
    path: '*',
    name: 'Error',
    component: Error
  }
]


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})





export default router

